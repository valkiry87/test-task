package skeleton;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Stepdefs {
    WebDriver webDriver;
    Google my;

    @Before
    public void setUp() {
        webDriver = new ChromeDriver();
    }

    @After
    public void quitBrowser() {
        webDriver.quit();
    }

    @Given("^I open google\\.de$")
    public void openPage() throws Throwable {

        my.open();
    }

    @When("^I input \"(.*?[^\"])\" into \"(.*[^\"])\"$")
    public void inputText(String text, String field) throws Throwable {
        my.inputText(text, field);
    }

    @When("^I press \"(.*?[^\"])\" button$")
    public void pressButton(String buttonName) throws Throwable {
        my.pressButton(buttonName);
    }

    @Then("^I should see \"(.*?[^\"])\" text in \"(.*?[^\"])\"$")
    public void verifyText(String text, String field) throws Throwable {
        my.verifyText(text, field);
        webDriver.quit();
    }
}