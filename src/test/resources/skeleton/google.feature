Feature: Google Feature

	@1
    Scenario: Input correct text using “Google-Suche” button and check results
    Given I open google.de
    When I input “dog against cat” into “inputField”
    And I press “Google-Suche” button
    Then I should see “Bilder zu dog against cat” text in “pictureArea”
    @2
    Scenario: Click button “Auf gut Glück!” and check results
    Given I open google.de
    When I press “Auf gut Glück!” button
    Then I should see “Doodle-Archiv” text in “archiveLink”