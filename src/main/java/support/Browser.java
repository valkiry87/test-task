package support;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Browser {

    public static WebDriver launch() {
        String browser = System.getProperty("browser");
        if (browser.equals("chrome")) {
            return new ChromeDriver();
        } else {
            throw new RuntimeException("Unrecognized system property 'browser': " + browser);
        }
    }
}
