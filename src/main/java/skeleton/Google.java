package skeleton;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import support.Browser;

import static org.junit.Assert.assertEquals;

public class Google {
    public static final String url = "https:/google.de/";
    public By inputField = By.id("lst-ib");
    public String pictureArea = "//*[@id=\"imagebox_bigimages\"]/g-section-with-header/div[1]/h3/a";
    public String archiveLink = "//*[@id=\"archive-link\"]/a";
    WebDriver webDriver;


    public void open() {
        Browser.launch();
        webDriver.get(url);
    }

    public void inputText(String text, String field) {
        webDriver.findElement(By.xpath(field)).sendKeys(text);
    }

    public void pressButton(String buttonName) {
        webDriver.findElement(By.xpath("//*[@value=\"" + buttonName + "\"]")).click();
    }

    public void verifyText(String text, String idName) {
        WebElement actualText = webDriver.findElement(By.xpath(idName));
        assertEquals(actualText.getText(), text);
    }
}
